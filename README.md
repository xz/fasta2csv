fasta2csv
=========

Convert a FastA file into a CSV file.

Every record in the input FastA file will be converted into a row in the
output CSV file with three fields: "seqname", "description", and "seq".
Optionally, the length of each "seq" will be calculated and stored in the
"seqlen" column.

A FastA record begins with a header line that starts with ">". In the
header line, the string before the first space character is parsed as the
"seqname", and the rest of the line is parsed as the "description". The
subsequent lines are concatenated and parsed as "seq", until the next
occurrence of a line that starts with ">".

Features
--------

The input fasta is read as a stream of records and the output CSV file is
written out as a stream of rows. This means that at any moment, the RAM only
needs enough space to store and process one record. It also works very well with
shell pipes. For example, you could stream out the CSV into STDOUT by omiting
the `--outpath` option, and view it immediately using a command-line csv viewer
like `xsv table` or `vll`.
