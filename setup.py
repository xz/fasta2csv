import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="fasta2csv",
    version="0.0.1",
    author="Xun Zhu",
    author_email="zhuxun2@gmail.com",
    description="Convert a FastA file into a CSV file.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/w9/fasta2csv",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
        'fire',
        'tqdm',
    ],
    scripts=[
        'scripts/fasta2csv',
    ],
)
