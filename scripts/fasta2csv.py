#!/usr/bin/env python3

import sys
import os
import fire
import csv
from tqdm import tqdm

def main(inpath, outpath=None, seqlen=True):
    """Convert a FastA file into a CSV file.

    Every record in the input FastA file will be converted into a row in the
    output CSV file with three fields: "seqname", "description", and "seq".

    A FastA record begins with a header line that starts with ">". In the
    header line, the string before the first space character is parsed as the
    "seqname", and the rest of the line is parsed as the "description". The
    subsequent lines are concatenated and parsed as "seq", until the next
    occurrence of a line that starts with ">".

    Args:
        inpath: (path or filename) The path to the input FastA file
        outpath: (path or filename) The path to the output CSV file, if unspecified, will output to STDOUT
        seqlen: (flag) wether to add a column that counts the length of the seq in each record
    """
    if outpath is None:
        outfile = sys.stdout
    else:
        outfile = open(outpath, "w")

    filesize = os.path.getsize(inpath)

    records = []
    with open(inpath, "r") as infile:
        if seqlen:
            fieldnames = ["seqname", "description", "seqlen", "seq"]
        else:
            fieldnames = ["seqname", "description", "seq"]

        writer = csv.DictWriter(outfile, fieldnames=fieldnames)
        writer.writeheader()

        record = None
        seq = None
        if outpath is None:
            pbar = tqdm(total=filesize)
        for line in infile:
            if outpath is None:
                pbar.update(len(line))
            if line.startswith(">"):
                if record is not None:
                    record["seq"] = seq
                    if seqlen:
                        record["seqlen"] = len(seq)
                    writer.writerow(record)
                    records.append(record)

                seqname, _, description = line.rstrip()[1:].partition(' ')
                record = {
                    "seqname": seqname,
                    "description": description,
                }
                seq = ""
            else:
                if seq is None:
                    raise SyntaxError("The input file begins with a line that does not start with \">\".")
                seq += line.rstrip()

        if record is not None:
            record["seq"] = seq
            writer.writerow(record)
            records.append(record)

        if outpath is None:
            pbar.close()


if __name__ == "__main__":
    fire.Fire(main)
